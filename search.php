<?php get_header(); ?>

<main class="container section-padding">

	<div class="row">
			
			<?php 

			if(have_posts()):

				while(have_posts()): the_post();

					get_template_part('template-parts/content');

				endwhile;


			else: 

				get_template_part('template-parts/content', 'none');

			endif;

			?>

	</div>

	<?php the_posts_pagination( array(
			'prev_text' => __( 'Previous page', 'mogo' ),
						'next_text' => __( 'Next page', 'mogo' ),
					)); ?>

</main>


<?php get_footer(); ?>
