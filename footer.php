<section class="map section-padding">
	<img src="<?php echo get_template_directory_uri().'/assets/img/localization.png'; ?>" alt="map icon">
	<h1><a href="#">Open Map</a></h1>
	<div class="line line-red"></div>
</section>

<footer class="footer section-padding">
	<div class="container">
		<div class="row">
			<div class="footer-info">
				<h1><?php bloginfo('title'); ?></h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<span>15k followers</span>
				<hr class="main-hr">
				<div class="follow">
					<span>Follow Us: </span>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/fb-blue.png'; ?>" alt="fb icon"></a>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/twitter-blue.png'; ?>" alt="fb icon"></a>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/insta-blue.png'; ?>" alt="fb icon"></a>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/tumblr-blue.png'; ?>" alt="fb icon"></a>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/google-blue.png'; ?>" alt="fb icon"></a>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/pin-blue.png'; ?>" alt="fb icon"></a>
					<a href="#"><img src="<?php echo get_template_directory_uri().'/assets/img/youtube-blue.png'; ?>" alt="fb icon"></a>
				</div>
				<form class="newsletter-form">
					<input type="text" placeholder="Your Email...">
					<button>Subscribe</button>
				</form>
			</div>
			<div class="footer-blog">
				<h1>Blogs</h1>
				<div class="row">
					<?php

					$footer_loop = new WP_Query(array('posts_per_page' => 3));

					if($footer_loop->have_posts()):

						while($footer_loop->have_posts()): $footer_loop->the_post();
					?>
							<div class="footer-post row">
								<?php if(has_post_thumbnail()): ?>
									<div class="footer-post-thumbnail">
										<?php the_post_thumbnail(); ?>
									</div>
								<?php endif; ?>
								<div class="footer-post-content">
									<a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?>
									</a>
									<span><?php the_date(); ?></span>
								</div>
							</div>

						<?php endwhile; wp_reset_postdata(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<hr class="main-hr">
		<div class="copyright">
			<span>Copyright</span>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>