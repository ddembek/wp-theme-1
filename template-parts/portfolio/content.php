<div class="portfolio-photo" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
	<div class="example-photo-hover">
		<a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?></a>
	</div>
</div>
