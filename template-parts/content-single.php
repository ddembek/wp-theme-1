<article class="single-post">
	<header>
		<?php the_title('<h1 class="single-post-title">', '</h1>'); ?>
		<div class="line line-red"></div>
		<div class="single-post-tags"><?php the_tags('', ' | '); ?></div>
		<div class="single-post-category">
			<?php the_category(' | '); ?>
		</div>
	</header>
	<?php if(has_post_thumbnail()): ?>
		<div class="single-post-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div>
	<?php endif; ?>
	<div class="single-post-content">
		<?php the_content(); ?>
	</div>
</article>