<?php

if(!is_active_sidebar('sidebar-right')):
	return ;
endif;

?>

<aside class="sidebar-right">
	<?php dynamic_sidebar('sidebar-right'); ?>
</aside>