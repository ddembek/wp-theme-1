<?php get_header(); ?>

<main class="container section-padding">

	<div class="row">

		<div class="single-post-container">
	
			<?php
			
			while(have_posts()): the_post();

				get_template_part('template-parts/content', 'single');

			endwhile;

			if(comments_open() || get_comments_number()):

				comments_template(); 

			endif;

			?>

		</div>

		<?php get_sidebar('sidebar-right'); ?>

	</div>

</main>


<?php get_footer(); ?>