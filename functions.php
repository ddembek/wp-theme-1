<?php

if(!function_exists('mogo_setup')) {

	function mogo_setup() {

		add_theme_support('post-thumbnails');

		register_nav_menus(array(
			'main-menu' => __('Main Menu', 'mogo'),
		));
	}
}
add_action('after_setup_theme', 'mogo_setup');


function mogo_scripts() {

	wp_enqueue_style('mogo-style', get_stylesheet_uri());
	wp_enqueue_script('mogo-jquery', get_template_directory_uri().'/assets/js/jquery-3.2.1.min.js');
	wp_enqueue_script('mogo-html5', get_template_directory_uri().'/assets/js/html5shiv.min.js');
	wp_enqueue_script('mogo-mq', get_template_directory_uri().'/assets/js/mq.min.js');
	wp_enqueue_script('mogo-scripts', get_template_directory_uri().'/assets/js/scripts.js');
}
add_action('wp_enqueue_scripts', 'mogo_scripts');

function mogo_custom_post_types() {

	$portfolio_labels = array(
		'name' => __('Portfolio', 'brandi'),
		'singular_name' => 'portfolio',
	);

	$portfolio_args = array(
		'labels' => $portfolio_labels,
		'public' => true,
		'menu_icon' => 'dashicons-images-alt2',
		'supports' => array('title', 'editor', 'thumbnail'),
		'taxonomies' => array('portfolio_tags'),
	);

	register_post_type('portfolio', $portfolio_args);

}
add_action('init', 'mogo_custom_post_types');

function mogo_custom_taxonomies() {

	$portfolio_taxonomy_labels = array(
		'name' => __('Portfolio Tags', 'brandi'),
		'singular_name' => __('Portfolio Tag', 'brandi'),
	);

	$portfolio_taxonomy_args = array(
		'labels' => $portfolio_taxonomy_labels,
	);

	register_taxonomy('portfolio_tags', array('portfolio'), $portfolio_taxonomy_args);
}
add_action('init', 'mogo_custom_taxonomies');

function mogo_widgets_init() {
	register_sidebar( array(
		'name' => __('Right Sidebar', 'mogo'),
		'id' => 'sidebar-right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	) );
}

add_action('widgets_init', 'mogo_widgets_init');

show_admin_bar( false );


?>