<?php get_header(); ?>

<section class="container section-padding">
	<header class="section-header">
		<h1>What we do</h1>
		<h2>Story about us</h2>
	</header>
	<div class="line line-red"></div>
	<p class="section-p">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
	</p>
	<div class="row">
		<div class="example-photo">
			<img src="<?php echo get_template_directory_uri().'/assets/img/photo-1.jpg'; ?>" alt="photo-1">
			<div class="example-photo-hover">
				<h1 class="photo-hover-text">Lorem ipsum</h1>
			</div>
		</div>
		<div class="example-photo">
			<img src="<?php echo get_template_directory_uri().'/assets/img/photo-1.jpg'; ?>" alt="photo-1">
			<div class="example-photo-hover">
				<h1 class="photo-hover-text">Lorem ipsum</h1>
			</div>
		</div>
		<div class="example-photo">
			<img src="<?php echo get_template_directory_uri().'/assets/img/photo-1.jpg'; ?>" alt="photo-1">
			<div class="example-photo-hover">
				<h1 class="photo-hover-text">Lorem ipsum</h1>
			</div>
		</div>
	</div>
</section>

<section class="numbers">
	<div class="container row">
		<div class="number">
			<h1>42</h1>
			<p>Projects</p>
		</div>
		<div class="number">
			<h1>123</h1>
			<p>Happy Client</p>
		</div>
		<div class="number">
			<h1>42</h1>
			<p>Award Winner</p>
		</div>
		<div class="number">
			<h1>15</h1>
			<p>Cup of Coffe</p>
		</div>
		<div class="number">
			<h1>99</h1>
			<p>Members</p>
		</div>
	</div>
</section>

<section class="container section-padding">
	<header class="section-header">
		<h1>We work with</h1>
		<h2>Amazing Services</h2>
	</header>
	<div class="line line-red"></div>
	<div class="services row">
		<div class="service">
			<div class="row">
				<div class="service-icon">
					<img src="<?php echo get_template_directory_uri().'/assets/img/alarm.png'; ?>">
				</div>
				<div class="service-content">
					<h1>Photography</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
				</div>
			</div>
		</div>
		<div class="service">
			<div class="row">
				<div class="service-icon">
					<img src="<?php echo get_template_directory_uri().'/assets/img/line-graph.png'; ?>">
				</div>
				<div class="service-content">
					<h1>Web Design</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
				</div>
			</div>
		</div>
		<div class="service">
			<div class="row">
				<div class="service-icon">
					<img src="<?php echo get_template_directory_uri().'/assets/img/computer.png'; ?>">
				</div>
				<div class="service-content">
					<h1>Creativity</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
				</div>
			</div>
		</div>
		<div class="service">
			<div class="row">
				<div class="service-icon">
					<img src="<?php echo get_template_directory_uri().'/assets/img/book.png'; ?>">
				</div>
				<div class="service-content">
					<h1>SEO</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
				</div>
			</div>
		</div>
		<div class="service">
			<div class="row">
				<div class="service-icon">
					<img src="<?php echo get_template_directory_uri().'/assets/img/home.png'; ?>">
				</div>
				<div class="service-content">
					<h1>CSS/HTML</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
				</div>
			</div>
		</div>
		<div class="service">
			<div class="row">
				<div class="service-icon">
					<img src="<?php echo get_template_directory_uri().'/assets/img/image.png'; ?>">
				</div>
				<div class="service-content">
					<h1>Digital</h1>
					<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="design section-padding">
	<header class="section-header">
		<h1>For all devices</h1>
		<h2>Unique design</h2>
	</header>
	<div class="line line-red"></div>
	<div class="design-img">
		<img src="<?php echo get_template_directory_uri().'/assets/img/design.png'; ?>" alt="mobile">
	</div>
</section>

<section class="services-tabs container section-padding">
	<header class="section-header">
		<h1>Service</h1>
		<h2>What we do</h2>
	</header>
	<div class="line line-red"></div>
	<p class="section-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
	<div class="row">
		<div class="services-tabs-img">
			<img src="<?php echo get_template_directory_uri().'/assets/img/photo-1.jpg'; ?>" alt="example-photo">
		</div>
		<div class="tabs">
			<div class="tab">
				<header class="tab-header row">
					<img class="tab-float" src="<?php echo get_template_directory_uri().'/assets/img/picture.png'; ?>" alt="example-photo">
					<h1 class="tab-float">Photography</h1>
					<img class="arrow tab-float" src="<?php echo get_template_directory_uri().'/assets/img/arrow_down.png'; ?>" alt="example-photo">
				</header>
				<p class="tab-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			</div>
			<div class="tab">
				<header class="tab-header row">
					<img class="tab-float" src="<?php echo get_template_directory_uri().'/assets/img/equalizer.png'; ?>" alt="example-photo">
					<h1 class="tab-float">Creativity</h1>
					<img class="arrow tab-float" src="<?php echo get_template_directory_uri().'/assets/img/arrow_down.png'; ?>" alt="example-photo">
				</header>
				<p class="tab-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			</div>
			<div class="tab">
				<header class="tab-header row">
					<img class="tab-float" src="<?php echo get_template_directory_uri().'/assets/img/bullseye.png'; ?>" alt="example-photo">
					<h1 class="tab-float">Web Design</h1>
					<img class="arrow tab-float" src="<?php echo get_template_directory_uri().'/assets/img/arrow_down.png'; ?>" alt="example-photo">
				</header>
				<p class="tab-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
			</div>
		</div>
	</div>
</section>

<section class="testimonials-slideshow" id="testimonials-1">
	<div class="container">
		<?php echo do_shortcode('[testimonial_view id=3]'); ?>
	</div>
</section>

<section class="container section-padding">
	<header class="section-header">
		<h1>Who we are</h1>
		<h2>Meet our team</h2>
	</header>
	<div class="line line-red"></div>
	<p class="section-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
	<div class="row">
		<div class="person">
			<div class="example-photo">
				<img src="<?php echo get_template_directory_uri().'/assets/img/photographer.jpg'; ?>" alt="photo-1">
				<div class="example-photo-hover">
					<div class="team-social photo-hover-text">
						<a href="#" class="fb-icon"></a>
						<a href="#" class="twitter-icon"></a>
						<a href="#" class="pin-icon"></a>
						<a href="#" class="insta-icon"></a>
					</div>
				</div>
			</div>
			<div class="person-info">
				<h1>Matthew Dix</h1>
				<h2>Graphic Designer</h2>
			</div>
		</div>
		<div class="person">
			<div class="example-photo">
				<img src="<?php echo get_template_directory_uri().'/assets/img/photographer.jpg'; ?>" alt="photo-1">
				<div class="example-photo-hover">
					<div class="team-social photo-hover-text">
						<a href="#" class="fb-icon"></a>
						<a href="#" class="twitter-icon"></a>
						<a href="#" class="pin-icon"></a>
						<a href="#" class="insta-icon"></a>
					</div>
				</div>
			</div>
			<div class="person-info">
				<h1>Christopher Campbell</h1>
				<h2>Branding/UX design</h2>
			</div>
		</div>
		<div class="person">
			<div class="example-photo">
				<img src="<?php echo get_template_directory_uri().'/assets/img/photographer.jpg'; ?>" alt="photo-1">
				<div class="example-photo-hover">
					<div class="team-social photo-hover-text">
						<a href="#" class="fb-icon"></a>
						<a href="#" class="twitter-icon"></a>
						<a href="#" class="pin-icon"></a>
						<a href="#" class="insta-icon"></a>
					</div>
				</div>
			</div>
			<div class="person-info">
				<h1>Michael Fertig</h1>
				<h2>Developer</h2>
			</div>
		</div>
	</div>
</section>

<section class="portfolio section-padding">
	<div class="container">
		<header class="section-header">
			<h1>What we do</h1>
			<h2>Some of our work</h2>
		</header>
		<div class="line line-red"></div>
		<p class="section-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
	</div>
	<div class="portfolio row">
		<?php
		$portfolio_loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => 8));

		if($portfolio_loop->have_posts()):

			while($portfolio_loop->have_posts()): $portfolio_loop->the_post();
				
				get_template_part('template-parts/portfolio/content');

			endwhile; wp_reset_postdata();

		else:

			get_template_part('template-parts/portfolio/content', 'none');

		endif;
		?>
	</div>
</section>

<section class="testimonials-slideshow" id="testimonials-2">
	<div class="container">
		<?php echo do_shortcode('[testimonial_view id=3]'); ?>
	</div>
</section>

<section id="testimonials-3" class="section-padding">
	<div class="container">
		<header class="section-header">
			<h1>Who we are</h1>
			<h2>Meet our team</h2>
		</header>
		<div class="line line-red"></div>
		<?php echo do_shortcode('[testimonial_view id=4]'); ?>
	</div>
</section>

<section class="section-padding">
	<div class="container">
		<header class="section-header">
			<h1>Our Stories</h1>
			<h2>Latest Blog</h2>
		</header>
		<div class="line line-red"></div>

		<div class="row">
	
			<?php

			$latest_loop = new WP_Query(array('posts_per_page' => 3));

			if($latest_loop->have_posts()): 

				while($latest_loop->have_posts()): $latest_loop->the_post();

					get_template_part('template-parts/content');

				endwhile; wp_reset_postdata();

			else:

			?>
				<header class="fp-none">
					<h1>Currently there are no posts</h1>
				</header>

			<?php endif; ?>

		</div>
	</div>
</section>


<?php get_footer(); ?>