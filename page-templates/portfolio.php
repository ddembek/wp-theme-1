<?php

/*

	Template Name: Portfolio

*/

?>

<?php get_header(); ?>

<main class="section-padding">

	<header>
		<h1 class="page-header">Portfolio</h1>
	</header>

	<div class="line line-blue"></div>

		<div class="portfolio row">
			
			<?php 

			$portfolio_loop = new WP_Query(array('posts_per_page' => -1, 'post_type' => 'portfolio'));

			if($portfolio_loop->have_posts()):

				while($portfolio_loop->have_posts()): $portfolio_loop->the_post();

					get_template_part('template-parts/portfolio/content');

				endwhile;

			else:

				get_template_part('template-parts/portfolio/content', 'none');

			endif;

			?>

		</div>

</main>

<?php get_footer(); ?>