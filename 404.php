<?php get_header(); ?>

<main class="container section-padding">
	<header class="error-404">
		<h1>Page Not Found</h1>
	</header>
	<div class="line line-red"></div>
	<?php get_search_form(); ?>
</main>

<?php get_footer(); ?>