<article class="post">

	

		<?php if(has_post_thumbnail()): ?>

			<div class="post-thumbnail" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>');">

		<?php else: ?>

			<div class="post-thumbnail" style="background-image: url('<?php echo get_template_directory_uri().'/assets/img/photo-1.jpg'; ?>');">

		<?php endif; ?>

		<div class="post-date">

			<?php the_date("j M"); ?>

		</div>

	</div>


	<div class="post-container">
		
		<a href="<?php the_permalink(); ?>"><?php the_title('<h1 class="post-title">', '</h1>'); ?></a>

		<p class="post-content"><?php echo wp_trim_words(get_the_content(), 25); ?></p>


	</div>
	<hr class="main-hr">
	<div class="post-info">
		<div class="post-views"></div>
		<div class="post-comments">
			<img src="<?php echo get_template_directory_uri().'/assets/img/speech-buuble.png';?>">
			<?php comments_number(); ?>
		</div>
	</div>

</article>