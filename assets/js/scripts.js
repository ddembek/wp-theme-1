$(function() {

	function hoverPhotoEffect() {
		$('.example-photo').hover(function() {

			$(this).find('.example-photo-hover').show(300);

		});

		$('.example-photo').mouseleave(function() {
			$(this).find('.example-photo-hover').hide(300);
		});

		$('.portfolio-photo').hover(function() {

			$(this).find('.example-photo-hover').show(300);

		});

		$('.portfolio-photo').mouseleave(function() {

			$(this).find('.example-photo-hover').hide(300);

		});

	}

	if($(window).width() >= 992) {

		hoverPhotoEffect();
	}
	
	

	$('.tab').click(function() {


		var tab_content = $(this).find('.tab-content');
		var tab_content_display = tab_content.css('display');
		var arrow_img = $(this).find('.arrow');

		if(tab_content_display == 'none') {

			tab_content.show(300);
			arrow_img.css('transform', 'rotate(180deg)');

		} else {

			tab_content.hide(300);
			arrow_img.css('transform', 'rotate(0deg)');
		}

	});


});