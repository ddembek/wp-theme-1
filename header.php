<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	 
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="welcome">
		<nav class="container row">
			<div class="site-title">
				<h1><?php bloginfo('title'); ?></h1>
			</div>
			<div class="main-menu">
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
			</div>
		</nav>
		<div class="site-headers">
			<h1>Creative Template</h1>
			<h2>Welcome<br> to Mogo</h2>
			<div class="line line-white"></div>
			<h3>Learn More</h3>
		</div>
	</div>
	