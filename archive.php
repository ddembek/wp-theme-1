<?php get_header(); ?>


<main class="container section-padding">

	<header class="page-header">
		<h1><?php the_archive_title(); ?></h1>
		<div class="line line-blue"></div>
	</header>

	<div class="row">

		<?php if(have_posts()): ?>

			<?php while(have_posts()): the_post();

				get_template_part('template-parts/content');

				endwhile;

				the_posts_pagination( array(
					'prev_text' => __( 'Previous page', 'mogo' ),
					'next_text' => __( 'Next page', 'mogo' ),
				));


			else: 

				get_template_part('template-parts/content', 'none');

			endif;

			?>

	</div>
	
</main>


<?php get_footer(); ?>